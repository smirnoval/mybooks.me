import random
import string
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404
import haikunator
from chat.models import Room, ConnectedRooms, NewMessagesInRoom
from mainapp.models import Book
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist

def about(request):
    return render(request, "chat/about.html")

def new_room(request):
    """
    Randomly create a new room, and redirect to it.
    """
    new_room = None
    while not new_room:
        with transaction.atomic():
            label = haikunator.haikunate()
            if Room.objects.filter(label=label).exists():
                continue
            new_room = Room.objects.create(label=label)
    return redirect(chat_room, label=label)

def chat_room(request, label):
    book = get_object_or_404(Book, pk=label)
    room, created = Room.objects.get_or_create(label=label, id_book=book, name=book.name)
    try:
        connectedRoom = ConnectedRooms.objects.get(user=request.user, room=room)
        roomFlag = True
    except ObjectDoesNotExist:
        roomFlag = False

    # We want to show the last 50 messages, ordered most-recent-last
    messages = reversed(room.messages.order_by('-timestamp')[:50])

    return render(request, "chat/room.html", {
        'room': room,
        'messages': messages,
        'flag': roomFlag,
    })

def join_connect(request, label):
    try:
        room = Room.objects.get(id_book=label)
        room = ConnectedRooms.objects.get(user=request.user, room=room)
    except ObjectDoesNotExist:
        room = ConnectedRooms(user=request.user, room=room)
        room.save()
        messages = NewMessagesInRoom(room=room, quantity=0)
        messages.save()
    adress = '/chat/' + label
    return HttpResponseRedirect(adress)

def join_disconnect(request, label):
    try:
        room = Room.objects.get(id_book=label)
        room = ConnectedRooms.objects.get(user=request.user, room=room)
        messages = NewMessagesInRoom.objects.get(room=room)
        messages.delete()
        room.delete()
        adress = '/chat/' + label
        return HttpResponseRedirect(adress)
    except ObjectDoesNotExist:
        return HttpResponse("You are not connected to the room!")