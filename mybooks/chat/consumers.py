import re
import json
import logging
from django.shortcuts import get_object_or_404
from channels import Group
from channels.sessions import channel_session
from chat.models import Room, ConnectedRooms, NewMessagesInRoom
from django.contrib.auth.models import User
from channels.auth import http_session_user, channel_session_user, transfer_user, channel_session_user_from_http

log = logging.getLogger(__name__)

@channel_session_user_from_http
def ws_connect(message):
        # Notifications
    try:
        rooms = ConnectedRooms.objects.all().filter(user=message.user)
        log.debug('Getting room-%s', rooms)
        for i in rooms:
            log.debug('User name:-%s', message.user)
            log.debug('Room:-%s', i.room.label)
            Group('messages_chat-'+str(message.user), channel_layer=message.channel_layer).add(message.reply_channel)
            message.channel_session['messages_chat-'+str(message.user)] = i.room.label
    except:
        log.debug('It was message from chatroom')
    try:
        prefix, label = message['path'].decode('ascii').strip('/').split('/')
        if prefix != 'chat':
            log.debug('invalid ws path=%s', message['path'])
            return
        room = Room.objects.get(label=label)
    except ValueError:
        log.debug('invalid ws path=%s', message['path'])
        return
    except Room.DoesNotExist:
        log.debug('ws room does not exist label=%s', label)
        return
    log.debug('chat connect room=%s client=%s:%s',
        room.label, message['client'][0], message['client'][1])
    Group('chat-'+label, channel_layer=message.channel_layer).add(message.reply_channel)
    log.debug(message.reply_channel)
    log.debug(message.user)
    message.channel_session['room'] = room.label

    # Set zero unreaded messages on the view notification
    m = {"set_zero" : label}
    Group('messages_chat-'+str(message.user), channel_layer=message.channel_layer).send({'text': json.dumps(m)})

    # Set zero unreaded messages in db
    connected_room = ConnectedRooms.objects.get(user=message.user, room=room)
    log.debug('Number room=%s', connected_room.room)
    log.debug('User=%s', message.user)
    new_messages = NewMessagesInRoom.objects.get(room=connected_room)
    log.debug('Quantity of messages=%s',new_messages.quantity)
    new_messages.quantity = 0
    new_messages.connected = True
    new_messages.save()

@channel_session_user
def ws_receive(message):
    ### Send notifications on view   /chat/notifications
    if message:
        label = message.channel_session['room']
        room = Room.objects.get(label=label)
        connected_rooms = ConnectedRooms.objects.all().filter(room=room)
        messages = NewMessagesInRoom.objects.all().filter(room__in=connected_rooms)
        m = {"room" : label}
        for i in messages:
            if i.connected == False:
                try:
                    i.quantity += 1
                    i.save()
                    log.debug('quantity added!')
                    Group('messages_chat-'+str(i.room.user), channel_layer=message.channel_layer).send({'text': json.dumps(m)})
                except:
                    log.debug('Group does not exist!')

    ### Send notifications in ROOM-CHANNEL on view /chat/#
    log.debug(message.channel_session)
    try:
        label = message.channel_session['room']
        room = Room.objects.get(label=label)
    except KeyError:
        log.debug('no room in channel_session')
        return
    except Room.DoesNotExist:
        log.debug('recieved message, buy room does not exist label=%s', label)
        return
    try:
        data = json.loads(message['text'])
    except ValueError:
        log.debug("ws message isn't json text=%s", text)
        return
    if set(data.keys()) != set(('handle', 'message')):
        log.debug("ws message unexpected format data=%s", data)
        return
    if data:
        log.debug('chat message room=%s handle=%s message=%s',
            room.label, message.user, data['message'])
        data['handle'] = message.user
        m = room.messages.create(**data)

        Group('chat-'+label, channel_layer=message.channel_layer).send({'text': json.dumps(m.as_dict())})

@channel_session_user
def ws_disconnect(message):
    log.debug("User=%s try exit", message.user)
    try:
        label = message.channel_session['room']
        room = Room.objects.get(label=label)
        Group('chat-'+label, channel_layer=message.channel_layer).discard(message.reply_channel)
        connected_room = ConnectedRooms.objects.get(user=message.user, room=room)
        log.debug('Number leaving room=%s', connected_room.room)
        log.debug('Leaving User=%s', message.user)
        new_messages = NewMessagesInRoom.objects.get(room=connected_room)
        new_messages.connected = False
        new_messages.save()
    except:
        Group('messages_chat-'+str(message.user), channel_layer=message.channel_layer).discard(message.reply_channel)
