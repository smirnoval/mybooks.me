$(function() {
    var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    var chatsock = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host +  window.location.pathname);

    chatsock.onmessage = function(message) {
        var data = JSON.parse(message.data);
        if (data.room) {
            alert("Hi!");
            var channel = document.getElementById("channel-"+data.room);
            channel.textContent = "Пришло новое сообщение!";
            var quantity = document.getElementById("quantity-"+data.room);
            quantity.textContent = parseInt(quantity.textContent) + 1
        }
        else if (data.set_zero) {
            var channel = document.getElementById("channel-"+data.set_zero);
            channel.textContent = "Новых сообщений нет!";
            var quantity = document.getElementById("quantity-"+data.set_zero);
            quantity.textContent = 0
        }

    };
});