from __future__ import unicode_literals
from django.contrib.auth.models import User
from mainapp.models import Book
from django.db import models
from django.utils import timezone

class Room(models.Model):
    name = models.TextField()
    label = models.SlugField(unique=True)
    id_book = models.OneToOneField(Book, related_name='book')

    def __unicode__(self):
        return self.label


class Message(models.Model):
    room = models.ForeignKey(Room, related_name='messages')
    handle = models.ForeignKey(User, related_name='user')
    message = models.TextField()
    timestamp = models.DateTimeField(default=timezone.now, db_index=True)

    def __unicode__(self):
        return '[{timestamp}] {handle}: {message}'.format(**self.as_dict())

    @property
    def formatted_timestamp(self):
        return self.timestamp.strftime('%b %-d %-I:%M %p')

    def as_dict(self):
        return {'handle': self.handle.username, 'message': self.message, 'timestamp': self.formatted_timestamp}


class ConnectedRooms(models.Model):
    user = models.ForeignKey(User, related_name='user_in_room')
    room = models.ForeignKey(Room, related_name='room')


class NewMessagesInRoom(models.Model):
    room = models.OneToOneField(ConnectedRooms, related_name='room_notification')
    quantity = models.IntegerField('quantity')
    connected = models.BooleanField(default=False)
