# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

# Create your models here.

def content_author_photo_name(instance, filename):
    return '/'.join(["media", "images", "author", str(instance.pk), "avatar"])

class Author(models.Model):
    name = models.CharField('author_name', max_length=200)
    original_name = models.CharField('original_name', max_length=200)
    country = models.CharField('country', max_length=40)
    birth_date  = models.CharField('birth_date', max_length=20)
    bigraphy = models.TextField('biography')
    photo_url = models.ImageField(upload_to=content_author_photo_name, default="media/images/author/default.png")
    rating = models.TextField('rating', default="-")

    def __str__(self):
        return self.name.encode('utf8')

    def update_rating(self):
        books = Book.objects.all().filter(author=self.pk)
        rating = 0
        counter = 0
        for i in books:
            if i.rating == "-":
                pass
            else:
                rating += float(i.rating)
                counter += 1
        if rating != 0:
            rating = float(rating) / counter
            rating = round(rating, 4)
            self.rating = rating

def content_book_cover_name(instance, filename):
    return '/'.join(["media", "images", "book", str(instance.pk), "cover"])

class Book(models.Model):
    name = models.CharField('book_name', max_length=100)
    original_name = models.CharField('original_name', max_length=100)
    book_type = models.CharField('book_type', max_length=50)
    publish_year = models.CharField('publish_year', max_length=5)
    genre = models.CharField('genre', max_length=30)
    annotation = models.TextField('annotation')
    author = models.ManyToManyField(Author)
    cover_url = models.ImageField(upload_to=content_book_cover_name, default="media/images/book/default.png")
    rating = models.TextField('rating', default="-")

    def __str__(self):
        return self.name.encode('utf8')

    def update_rating(self):
        messages = MessageReading.objects.all().filter(book_id=self.pk)
        rating = 0
        for i in messages:
            rating += i.evaluation
        rating = float(rating) / len(messages)
        rating = round(rating, 4)
        self.rating = rating


class MessageReading(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    book_id = models.ForeignKey(Book, on_delete=models.CASCADE)
    add_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    evaluation  = models.PositiveSmallIntegerField('evaluation', null=True)
    BOOK_STATE = (
        ('1', 'Читаю сейчас'),
        ('2', 'Прочитал'),
        ('3', 'Хочу прочитать'),
        ('4', 'Перестал читать'),
    )
    status = models.CharField(max_length=2, null=True, choices=BOOK_STATE, default=2)

    def __str__(self):
        return str(self.user_id)+str(self.book_id)

class UserActions(models.Model):
    request_user = models.ForeignKey(User,  on_delete=models.CASCADE, related_name='request_user')
    action_user = models.ForeignKey(User,  on_delete=models.CASCADE, related_name='action_user')
    add_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    action = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.request_user)+str(self.action_user)+str(self.action)

def content_avatar_name(instance, filename):
    return '/'.join(["media", "images", "user", str(instance.user_id.pk), "avatar"])

class Profile(models.Model):
    user_id = models.OneToOneField(User)
    age = models.IntegerField(null=True)
    avatar = models.ImageField(upload_to=content_avatar_name, default="media/images/user/default.png")

    def __str__(self):
        return str(self.user_id)

class Subscribe(models.Model):
    from_user = models.ForeignKey(User)
    to_user = models.ForeignKey(User, related_name="person_subscriber")

    def __str__(self):
        return str(self.from_user)

class CommentForBook(models.Model):
    book_id = models.ForeignKey(Book)
    user_id = models.ForeignKey(User)
    add_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    comment_field = models.TextField('comment')

    def __str__(self):
        return str(self.book_id)