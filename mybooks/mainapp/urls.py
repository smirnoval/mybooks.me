from django.conf.urls import url
from mainapp import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register/', views.register),
    url(r'^login/$', views.ajax_login, name='ajax_login'),
    url(r'^logout/$', views.ajax_logout, name='ajax_logout'),

    #   Authors
    url(r'^author/add_author/$', views.add_new_author, name='add_new_author'),
    url(r'^authors/$', views.show_authors, name='show_authors'),
    url(r'^authors/?(?P<key>[A-Za-z_]+)/$', views.show_authors_sorted, name='show_authors_sorted'),
    url(r'^authors/(?P<pk>[0-9]+)/$', views.get_author, name='get_author'),
    url(r'^authors/(?P<pk>[0-9]+)/change/$', views.change_author, name='change_author'),

    # Books
    url(r'^authors/(?P<pk>[0-9]+)/add_book/$', views.add_new_book, name='add_new_book'),
    url(r'^books/$', views.all_books, name='all_books'),
    url(r'^books/?(?P<key>[A-Za-z_]+)/$', views.all_books_sorted, name='all_books_sorted'),
    url(r'^books/(?P<pk>[0-9]+)/$', views.get_book, name='get_book'),
    url(r'^books/(?P<pk>[0-9]+)/evaluation/$', views.give_evaluation, name='give_evaluation'),
    url(r'^books/(?P<pk>[0-9]+)/status/$', views.give_status, name='give_status'),
    url(r'^books/(?P<pk>[0-9]+)/add_comment$', views.add_comment, name='add_comment'),
    url(r'^books/(?P<pk>[0-9]+)/add_another_author$', views.add_another_author, name='add_another_author'),
    url(r'^books/(?P<pk>[0-9]+)/change/$', views.change_book, name='change_book'),
    url(r'^books/find_another_author$', views.find_another_author, name='find_another_author'),

    #   User profile and user activities
    url(r'^user/my_tape$', views.my_tape, name='my_tape'),
    url(r'^user/subscriber_tape$', views.subscriber_tape, name='subscriber_tape'),
    url(r'^change/profile/$', views.change_profile, name='change_profile'),
    url(r'^profile/(?P<pk>[0-9]+)/$', views.profile, name='profile'),
    url(r'^profile/(?P<pk>[0-9]+)/subscribe$', views.subscribe, name='subscribe'),
    url(r'^profile/(?P<pk>[0-9]+)/unsubscribe$', views.unsubscribe, name='unsubscribe'),

    #   Additional options
    url(r'^export/$', views.export_books_from_profile, name='export_books_from_profile'),
    url(r'^heat_map_(?P<pk>[0-9]+)/$', views.get_statistics, name='get_statistics'),
    url(r'^search/$', views.search, name='search'),

    #   Test features
    url(r'^send_test_mail/$', views.send_test_mail, name='send_test_mail'),
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)