# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.template import Context, loader
from django.http import HttpResponse
from mainapp.forms import RegistrationForm
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from mainapp.forms import AuthorForm, AuthorImageForm, BookForm, BookImageForm, ProfileForm, ProfileImageForm
from mainapp.models import Author, Book, MessageReading, Profile, UserActions, Subscribe, CommentForBook
from chat.models import ConnectedRooms, NewMessagesInRoom
from django.core.exceptions import ObjectDoesNotExist
from chat.models import ConnectedRooms
from chat.consumers import ws_connect
from itertools import chain
from datetime import datetime
from django.contrib.auth.decorators import login_required
import csv
import time
import datetime
import json
from mybooks.settings import EMAIL_HOST_USER
from django.core.mail import send_mail

# Create your views here.
def index(request):
    context_dic = {}
    context_dic['user'] = request.user
    if request.user.is_authenticated():
        return HttpResponseRedirect('/profile/'+str(request.user.pk))
    else:
        template = loader.get_template('mainapp/logout_main.html')
        return HttpResponse(template.render(context_dic))

@csrf_protect
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1'],
            email=form.cleaned_data['email']
            )
            send_mail('Добро пожаловать!', 'MyBooks приветствует вас!', EMAIL_HOST_USER, [form.cleaned_data['email']])
            last_user = User.objects.latest('id')
            profile = Profile(user_id = last_user)
            profile.save()
            return HttpResponseRedirect('/')
    else:
        form = RegistrationForm()
    variables = RequestContext(request, { 'form': form })
    return render_to_response( 'mainapp/register.html', variables,)

def ajax_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponse("Success!")
        else:
            return HttpResponse("Fail!")
    else:
        return HttpResponse("You are invalid! This account not exist!")

def ajax_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required
def add_new_author(request):
    if request.method == "POST":
        form = AuthorForm(request.POST)
        if form.is_valid():
            author = form.save(commit=False)
            author.save()
            return HttpResponseRedirect('/')
    else:
        form = AuthorForm()
    context = {}
    try:
        context = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context['user'] = request.user
    context['form'] = form
    request_context = RequestContext(request, context)
    return render_to_response( 'mainapp/add_new_author.html', request_context)

@login_required
def change_author(request,pk):
    if request.method == "POST":
        if request.FILES:
            form = AuthorImageForm(request.POST, request.FILES)
            author = Author.objects.get(pk=pk)
            if form.is_valid():
                if author.photo_url != "media/images/author/default.png":
                    author.photo_url.delete()
                author.photo_url = request.FILES['image']
                author.save()
                path = "/authors/" + str(pk)
                return HttpResponseRedirect(path)
    else:
        context_dic = {}
        context_dic['user'] = request.user
        request_context = RequestContext(request, context_dic)
        return render_to_response('mainapp/add_author_information.html', request_context)

@login_required
def show_authors(request):
    authors = Author.objects.order_by('id')
    template = loader.get_template('mainapp/authors.html')
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['authors'] = authors
    return HttpResponse(template.render(context_dic))

@login_required
def show_authors_sorted(request, key):
    try:
       authors = Author.objects.order_by(key)[::-1]
    except:
        authors = Author.objects.order_by('id')
    template = loader.get_template('mainapp/authors.html')
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['authors'] = authors
    return HttpResponse(template.render(context_dic))

@login_required
def get_author(request, pk):
    author = get_object_or_404(Author, pk=pk)
    author.update_rating()
    author.save()
    books = Book.objects.filter(author=pk)
    messages_user = MessageReading.objects.all().filter(user_id=request.user, book_id__in=books)
    for i in books:
        for j in messages_user:
            if i.pk == j.book_id.pk:
                i.evaluation = j.evaluation
    template = loader.get_template('mainapp/author.html')
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['author'] = author
    context_dic['range'] = range(1,6)
    context_dic['books'] = books
    return HttpResponse(template.render(context_dic))

@login_required
def add_new_book(request, pk):
    if request.method == "POST":
        form = BookForm(request.POST)
        if form.is_valid():
            book = form.save(commit=False)
            author_id = get_object_or_404(Author, pk=pk)
            book.save()
            book.author.add(author_id)
            path = "/authors/" + str(pk)
            return HttpResponseRedirect(path)
    else:
        form = BookForm()
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['form'] = form
    request_context = RequestContext(request, context_dic)
    return render_to_response( 'mainapp/add_new_book.html', request_context)

@login_required
def change_book(request, pk):
    if request.method == "POST":
        if request.FILES:
            form = BookImageForm(request.POST, request.FILES)
            book = Book.objects.get(pk=pk)
            if form.is_valid():
                if book.cover_url != "media/images/book/default.png":
                    book.cover_url.delete()
                book.cover_url = request.FILES['image']
                book.save()
                path = "/books/" + str(pk)
                return HttpResponseRedirect(path)
    else:
        context_dic = {}
        context_dic['user'] = request.user
        request_context = RequestContext(request, context_dic)
        return render_to_response('mainapp/add_book_information.html', request_context)

@login_required
def add_another_author(request, pk):
    book = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        id_author = request.POST['id_author']
        author = get_object_or_404(Author, pk=id_author)
        book.author.add(author)
        path = "/books/" + str(pk)
        return HttpResponse("Success!")
    else:
        return HttpResponse("Error!")

@login_required
def find_another_author(request):
    if request.method == "POST":
        name_author = request.POST['name_author']
        authors = Author.objects.all().filter(name__icontains=name_author) | Author.objects.all().filter(original_name__icontains=name_author)
        data = [{'name': i.name, 'pk': i.pk} for i in authors]
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        return HttpResponse("Error!")

@login_required
def all_books(request):
    books = Book.objects.order_by('id')
    template = loader.get_template('mainapp/books.html')
    try:
        messages_user = MessageReading.objects.all().filter(user_id=request.user)
        for i in books:
            for j in messages_user:
                if i.pk == j.book_id.pk:
                    i.evaluation = j.evaluation
    except:
        print("User doesn't have books!")

    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['books'] = books
    context_dic['range'] = range(1,6)
    return HttpResponse(template.render(context_dic))

@login_required
def all_books_sorted(request, key):
    try:
        books = Book.objects.order_by(key)[::-1]
    except:
        books = Book.objects.order_by('id')

    try:
        messages_user = MessageReading.objects.all().filter(user_id=request.user)
        for i in books:
            for j in messages_user:
                if i.pk == j.book_id.pk:
                    i.evaluation = j.evaluation
    except:
        print("User doesn't have books!")

    template = loader.get_template('mainapp/books.html')
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['books'] = books
    context_dic['range'] = range(1,6)
    return HttpResponse(template.render(context_dic))

@login_required
def get_book(request, pk):
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    book = get_object_or_404(Book, pk=pk)
    comments = CommentForBook.objects.all().filter(book_id=pk)
    try:
        message = MessageReading.objects.get(user_id=request.user,book_id=book)
        context_dic['message'] = message
    except ObjectDoesNotExist:
        pass

    try:
        subsribers = Subscribe.objects.filter(from_user=request.user)
        list_subscribe_id = []
        for i in subsribers:
            list_subscribe_id.append(i.to_user.id)
        message_from_subs = MessageReading.objects.filter(user_id__in=list_subscribe_id,book_id=book)
        context_dic['message_from_subs'] = message_from_subs
    except:
        print("ERRRORR!")

    template = loader.get_template('mainapp/book.html')
    context_dic['user'] = request.user
    context_dic['book'] = book
    context_dic['comments'] = comments
    context_dic['range'] = range(1,6)
    context_dic['status'] = range(1,5)
    return HttpResponse(template.render(context_dic))

@login_required
def give_evaluation(request, pk):
    if request.method == "POST":
        evaluation = request.POST["eval"]
        try:
            book = get_object_or_404(Book, pk=pk)
            message = MessageReading.objects.get(user_id=request.user,book_id=book)
            message.evaluation = evaluation
            message.add_date =  datetime.now()
            message.save()
            book.update_rating()
            book.save()
            return HttpResponse("Success!")
        except ObjectDoesNotExist:
            message = MessageReading(user_id=request.user, book_id=book, evaluation=evaluation)
            message.save()
            book.update_rating()
            book.save()
            return HttpResponse("Success!")
    else:
        return HttpResponse("Go away, terrorist!")

@login_required
def give_status(request, pk):
    if request.method == "POST":
        status = request.POST["status"]
        try:
            book = get_object_or_404(Book, pk=pk)
            message = MessageReading.objects.get(user_id=request.user,book_id=book)
            message.status = status
            message.save()
            return HttpResponse("Success!")
        except ObjectDoesNotExist:
            message = MessageReading(user_id=request.user, book_id=book, status=status)
            message.save()
            return HttpResponse("Success!")
    else:
        return HttpResponse("Go away, terrorist!")

@login_required
def my_tape(request):
    book_actions = MessageReading.objects.all().filter(user_id=request.user)
    user_actions = UserActions.objects.all().filter(request_user=request.user) | UserActions.objects.all().filter(action_user=request.user)
    comment_actions = CommentForBook.objects.all().filter(user_id=request.user)
    actions = sorted(
        chain(book_actions, user_actions, comment_actions),
        key=lambda action: action.add_date, reverse=True)
    actions = actions[:15]
    template = loader.get_template('mainapp/my_tape.html')
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['actions'] = actions
    context_dic['range'] = range(1,6)
    return HttpResponse(template.render(context_dic))

@login_required
def change_profile(request):
    if request.method == "POST":
        if request.FILES:
            form = ProfileImageForm(request.POST, request.FILES)
            profile = Profile.objects.get(user_id=request.user.id)
            if form.is_valid():
                if profile.avatar != "media/images/user/default.png":
                    profile.avatar.delete()
                profile.avatar = request.FILES['image']
                profile.save()
                path = "/profile/" + str(request.user.pk)
                return HttpResponseRedirect(path)
        else:
            form = ProfileForm(request.POST)
            profile = Profile.objects.get(user_id=request.user.id)
            if form.is_valid():
                profile.age = form.cleaned_data['age'];
                profile.save()
                path = "/profile/" + str(request.user.pk)
                return HttpResponseRedirect(path)
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    profile = Profile.objects.get(user_id=request.user.id)
    context_dic['profile'] = profile
    context_dic['user'] = request.user
    context = Context(context_dic)
    request_context = RequestContext(request, context)
    return render_to_response('mainapp/add_profile_information.html', request_context)

@login_required
def profile(request, pk):
    profile = Profile.objects.get(user_id=pk)
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    try:
        books = MessageReading.objects.all().filter(user_id=pk)
        books_read = books.filter(status=1)
        books_complete = books.filter(status=2)
        books_want = books.filter(status=3)
        books_stop = books.filter(status=4)
        context_dic['books_read'] = books_read
        context_dic['books_complete'] = books_complete
        context_dic['books_want'] = books_want
        context_dic['books_stop'] = books_stop
        context_dic['range'] = range(1,6)
    except ObjectDoesNotExist:
        print("OOOGH!")
        pass
    user_account = User.objects.get(id=pk)
    subscribe_viewed_user = Subscribe.objects.all().filter(from_user=user_account)
    try:
        Subscribe.objects.get(from_user=request.user, to_user=user_account)
        subscribe_action = False
    except ObjectDoesNotExist:
        subscribe_action = True
    template = loader.get_template('mainapp/profile.html')
    context_dic['user'] = request.user
    context_dic['user_account'] = user_account
    context_dic['subscribe_action'] = subscribe_action
    context_dic['profile'] = profile
    context_dic['subscribe_viewed_user'] = subscribe_viewed_user
    return HttpResponse(template.render(context_dic))

@login_required
def subscribe(request, pk):
    user_for_subscribe =  User.objects.get(id=pk)
    try:
        subscribe = Subscribe.objects.get(from_user=request.user, to_user=user_for_subscribe)
        return HttpResponse("Error! You are already subscribe")
    except ObjectDoesNotExist:
        subscribe = Subscribe(from_user=request.user, to_user=user_for_subscribe)
        subscribe.save()
        action = UserActions(request_user=request.user, action_user=user_for_subscribe, action="subscribe")
        action.save()
        return HttpResponse("You subscribe")
    return HttpResponse("You are don't see this message! Sorry!")

@login_required
def unsubscribe(request, pk):
    user_for_unsubscribe =  User.objects.get(id=pk)
    try:
        subscribe = Subscribe.objects.get(from_user=request.user, to_user=user_for_unsubscribe)
        subscribe.delete()
        action = UserActions(request_user=request.user, action_user=user_for_unsubscribe, action="unsubscribe")
        action.save()
        return HttpResponse("You delete subscribe. Now you unsubscribe")
    except ObjectDoesNotExist:
        return HttpResponse("You unsubscribe for this user")
    return HttpResponse("You are don't see this message! Sorry!")

@login_required
def subscriber_tape(request):
    subscribe = Subscribe.objects.all().filter(from_user=request.user)
    list_subscribe_id = []
    for i in subscribe:
        list_subscribe_id.append(i.to_user.id)

    subscriber_actions_with_users = UserActions.objects.all().filter(request_user__in=list_subscribe_id)
    subscriber_actions_with_books = MessageReading.objects.all().filter(user_id__in=list_subscribe_id)
    subscriber_actions_with_reviews = CommentForBook.objects.all().filter(user_id__in=list_subscribe_id)
    actions = sorted(
        chain(subscriber_actions_with_users, subscriber_actions_with_books, subscriber_actions_with_reviews),
        key=lambda action: action.add_date, reverse=True)
    actions = actions[:30]
    template = loader.get_template('mainapp/subscribers.html')
    context_dic = {}
    try:
        context_dic = get_notifications(request)
    except:
        print("Error! Can't show notifications!")
    context_dic['user'] = request.user
    context_dic['actions'] = actions
    context_dic['range'] = range(1,6)
    return HttpResponse(template.render(context_dic))

@login_required
def add_comment(request, pk):
    book = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        comment_field = request.POST['comment']
        comment = CommentForBook(book_id=book, user_id=request.user, comment_field=comment_field)
        comment.save()
        return HttpResponse("You add comment!")
    else:
        return HttpResponse("Go away, terrorist!")

def get_notifications(request):
    context_dic = {}
    rooms = ConnectedRooms.objects.all().filter(user=request.user)
    messages = NewMessagesInRoom.objects.all().filter(room__in=rooms)
    context_dic['rooms_notif'] = rooms
    context_dic['messages_notif'] = messages
    return context_dic

def export_books_from_profile(request):
    books = MessageReading.objects.all().filter(user_id=request.user.pk)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="export_books_'+str(request.user.pk)+'.csv"'

    writer = csv.writer(response)
    for i in books:
        author_name = ""
        author_original_name = ""
        for j in i.book_id.author.all():
            author_name += j.name + " "
            author_original_name += j.original_name + " "
        writer.writerow([
            author_name.encode("utf_8"),
            author_original_name.encode("utf_8"),
            i.book_id.name.encode("utf_8"),
            i.book_id.original_name.encode("utf_8"),
            i.book_id.book_type.encode("utf_8"),
            i.book_id.publish_year.encode("utf_8"),
            i.book_id.genre.encode("utf_8"),
            i.evaluation,
            i.get_status_display().encode("utf_8")
            ])
    return response

def get_statistics(request, pk):
    books = MessageReading.objects.all().filter(user_id=pk)
    data = {}
    for i in books:
        temp_time = i.add_date.strftime("%Y%m%d")
        if temp_time in data:
            data[temp_time] += 1
        else:
            data[temp_time] = 1
    data_sec = {}
    for i in data:
        year = i[:4]
        mounth = i[4:6]
        day = i[6:]
        data_time = datetime.date(int(year), int(mounth), int(day))
        delta = int((data_time-datetime.date(1970,1,1)).total_seconds())
        data_sec[delta] = data[i]
    return HttpResponse(json.dumps(data_sec), content_type='application/json')

def search(request):
    context_dic = {}
    if request.method == "POST":
        search_field = request.POST['search_field']
        authors = Author.objects.all().filter(name__icontains=search_field ) | Author.objects.all().filter(original_name__icontains=search_field )
        books = Book.objects.all().filter(name__icontains=search_field ) | Book.objects.all().filter(original_name__icontains=search_field )

        data = {}
        for i in range(len(books)):
            author_name_in_book = ""
            for j in books[i].author.all():
                author_name_in_book += j.name + " "
            books[i].author_all = author_name_in_book

        data["authors"] = [
            {'name': i.name, 'original_name': i.original_name,
            'country': i.country, 'rating': i.rating, 'pk': i.pk}
            for i in authors]
        data["books"] = [
            {'name': i.name, 'original_name': i.original_name, 'author': i.author_all,
            'publish_year': i.publish_year, 'rating': i.rating, 'pk': i.pk}
            for i in books]
        return HttpResponse(json.dumps(data), content_type='application/json')
    else:
        context_dic['user'] = request.user
    template = loader.get_template('mainapp/search.html')
    return HttpResponse(template.render(context_dic))

def send_test_mail(request):
    send_mail('Добро пожаловать!', 'MyBooks приветствует вас!', EMAIL_HOST_USER, ["maintheme11@gmail.com"])
    return HttpResponse("E-mail send")