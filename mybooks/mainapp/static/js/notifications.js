$(function() {
    var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
    var chatsock = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host +  window.location.pathname);

    chatsock.onmessage = function(message) {
        var data = JSON.parse(message.data);
        if (data.room) {
            var notif_label = document.getElementById("notification_label");
            notif_label.textContent = "Уведомления +1";
            notif_label.style.color = "blue";
            var quantity = document.getElementById("quantity-"+data.room);
            quantity.textContent = parseInt(quantity.textContent) + 1
        }
        else if (data.set_zero) {
            var quantity = document.getElementById("quantity-"+data.set_zero);
            quantity.textContent = 0
        }

    };
});