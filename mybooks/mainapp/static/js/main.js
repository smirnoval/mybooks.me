$(document).ready(function() {
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false,
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    setTimeout(addEventListeners, 500);
    setTimeout(add_rate, 500);
    setTimeout(add_status, 500);
    setTimeout(notification_back, 500);
    show_heat_map();

});

function show_heat_map() {
    var heat_map = document.getElementById('cal-heatmap');
    if(heat_map) {
        var loc = location.href;
        var arr = loc.split('/');
        pk = arr[arr.length-2];
        var now_date = new Date();
        var cal = new CalHeatMap();
        cal.init({
            itemName: ["book", "books"],
            data: "http://127.0.0.1:8000/heat_map_"+pk,
            domain: "month",
            start: new Date(now_date.getFullYear()-1, now_date.getMonth()+1),
            subDomain: "day",
            range: 12,
            legend: [1, 2, 3, 4, 7, 15],
            displayLegend: true
        });
    }
}

function notification_back() {
    $("#notification_label").on('click', function(){
        var notif_label = document.getElementById("notification_label");
        notif_label.textContent = "Уведомления";
        notif_label.style.color = "gray";
    });
}

function add_rate() {
    $(".star").on('click', function(){
        var id = $(this).attr('id');
        var index = id.indexOf('_');
        var book_id = id.substring(0, index);
        var evaluate = id.substring(index+1, id.length);

        $.post("/books/"+book_id+"/evaluation"+"/", {
            eval: evaluate
        },
        function(data) {
            if(data == "Success!") {
                var rating = document.getElementById(book_id);
                rating.innerHTML = '';
                for(var i = 1; i < 6; i++) {
                    if(i<=evaluate) {
                        var li_rating = document.createElement('li');
                        li_rating.id = book_id + "_" + i;
                        li_rating.className = "star pick";
                        rating.appendChild(li_rating);
                    }
                    else {
                        var li_rating = document.createElement('li');
                        li_rating.id = book_id + "_" + i;
                        li_rating.className = "star";
                        rating.appendChild(li_rating);
                    }
                }
                add_rate()
            }
            else {
                alert("Fail!")
            }
        });
    });
}

function add_status() {
    $(".btn").on('click', function(){
        var id = $(this).attr('id');
        var index = id.indexOf('_');
        var book_id = id.substring(0, index);
        var status = id.substring(index+1, id.length);

        $.post("/books/"+book_id+"/status"+"/", {
            status: status
        },
        function(data) {
            if(data == "Success!") {
                var categories = document.getElementById("categories");
                categories.innerHTML = '';
                var text_mass = ["Читаю", "Прочитал", "Хочу прочитать", "Бросил"];
                for(var i = 1; i < 5; i++) {
                    if(i==status) {
                        var button = document.createElement('div');
                        button.id = book_id + "_" + i;
                        button.className = "btn btn-color";
                        button.textContent = text_mass[i-1]
                        categories.appendChild(button);
                    }
                    else {
                        var button = document.createElement('div');
                        button.id = book_id + "_" + i;
                        button.className = "btn";
                        button.textContent = text_mass[i-1]
                        categories.appendChild(button);
                    }
                }
                add_status();
            }
            else {
                alert("Fail!");
            }
        });
    });
}

function addEventListeners() {

    var ajax_login = document.getElementById('ajax_login');
    if(ajax_login) {
        ajax_login.addEventListener("click", ajaxLogin, false);
    }

    var ajax_logout = document.getElementById('ajax_logout');
    if(ajax_logout) {
        ajax_logout.addEventListener("click", ajaxLogout, false);
    }

    var ajax_logout = document.getElementById('ajax_logout');
    if(ajax_logout) {
        ajax_logout.addEventListener("click", ajaxLogout, false);
    }

    var add_comment = document.getElementById('ajax_add_comment');
    if(add_comment) {
        add_comment.addEventListener("click", addComment, false);
    }

    var add_another_author = document.getElementById('post_another_author');
    if(add_another_author) {
        add_another_author.addEventListener("click", addAnotherAuthor, false);
    }

    var find_another_author = document.getElementById('find_author');
    if(find_another_author) {
        find_another_author.addEventListener("click", findAnotherAuthor, false);
    }

    var search_button = document.getElementById('search_button');
    if(search_button) {
        search_button.addEventListener("click", searchAuthorsAndBooks, false);
    }

}

function ajaxLogin() {
    $.post("/login/", {
        username: $("#id_username").val(), password: $("#id_password").val()
    },
    function(data) {
        //alert(data)
        if (data == "Success!") {
            location.reload(true)
        }
        else {
            alert("Перезагрузка не удалась!")
        }

    });
}

function ajaxLogout() {
    $.post("/logout/", {
    },
    function(data) {
        location = "/";
        // // location.reload(true);
        // //alert("Вы вышли!");
    });
}

function addComment() {
    var loc = location.href;
    var arr = loc.split('/');
    loc = "";
    for (var i = 0; i < arr.length-1; i++) {
      loc += arr[i]+"/";
    }
    $.post(loc + 'add_comment', {
        comment: $("#id_add_comment").val()
    },
    function(data) {
        location.reload(true);
    });
}

function addAnotherAuthor() {
    var location_page = (location.href.substring(0, location.href.length - 5))
    $.post(location_page + 'add_another_author', {
        id_author: $("#id_author").val()
    },
    function(data) {
        if (data == "Success!") {
            location.reload(true)
        }
        else {
            alert(data)
        }
    });
}

function findAnotherAuthor() {
    $.post('/books/find_another_author', {
        name_author: $("#name_author").val()
    },
    function(data) {
        if (data) {
            var found_authors = document.getElementById("found_authors");
            found_authors.textContent = "";
            for(var i = 0; i < data.length; i++) {
                var row = document.createElement('div');
                row.className = "row";
                found_authors.appendChild(row)
                var two_cell = document.createElement('div');
                two_cell.className = "col-2";
                row.appendChild(two_cell);
                var six_cell = document.createElement('div');
                six_cell.className = "col-6";
                var ref = document.createElement('a');
                ref.href = "/authors/"+data[i].pk;
                ref.textContent = data[i].name;
                six_cell.appendChild(ref);
                row.appendChild(six_cell);
                var four_cell = document.createElement('div');
                four_cell.className = "col-4";
                four_cell.textContent = data[i].pk;
                row.appendChild(four_cell);
                var two_cell_2 = document.createElement('div');
                two_cell_2.className = "col-2";
                row.appendChild(two_cell_2);
            }
        }
        else {
            alert("RRR")
        }
    });
}

function searchAuthorsAndBooks() {
    $.post('/search/', {
        search_field: $("#search_field").val()
    },
    function(data) {
        if (data) {
            var search_authors = document.getElementById("search_authors");
            search_authors.textContent = "";
            var search_books = document.getElementById("search_books");
            search_books.textContent = "";
            if(data.authors.length > 0){
                for(var i = 0; i < data.authors.length; i++) {
                    var row = document.createElement('div');
                    row.className = "row";
                    search_authors.appendChild(row)

                    var two_cell_1 = document.createElement('div');
                    two_cell_1.className = "col-1";
                    row.appendChild(two_cell_1);

                    var two_cell_2 = document.createElement('div');
                    two_cell_2.className = "col-3";
                    var ref_2 = document.createElement('a');
                    ref_2.href = "/authors/"+data.authors[i].pk;
                    ref_2.textContent = data.authors[i].name;
                    two_cell_2.appendChild(ref_2);
                    row.appendChild(two_cell_2);

                    var two_cell_3 = document.createElement('div');
                    two_cell_3.className = "col-3";
                    var ref_3 = document.createElement('a');
                    ref_3.href = "/authors/"+data.authors[i].pk;
                    ref_3.textContent = data.authors[i].original_name;
                    two_cell_3.appendChild(ref_3);
                    row.appendChild(two_cell_3);

                    var two_cell_4 = document.createElement('div');
                    two_cell_4.className = "col-2";
                    two_cell_4.textContent = data.authors[i].country;
                    row.appendChild(two_cell_4);

                    var two_cell_5 = document.createElement('div');
                    two_cell_5.className = "col-2";
                    two_cell_5.textContent = data.authors[i].rating;
                    row.appendChild(two_cell_5);

                    var two_cell_6 = document.createElement('div');
                    two_cell_6.className = "col-1";
                    row.appendChild(two_cell_6);
                }
            }

            if(data.books.length > 0){
                for(var i = 0; i < data.books.length; i++) {
                    var row = document.createElement('div');
                    row.className = "row";
                    search_books.appendChild(row)

                    var two_cell_1 = document.createElement('div');
                    two_cell_1.className = "col-1";
                    row.appendChild(two_cell_1);

                    var two_cell_2 = document.createElement('div');
                    two_cell_2.className = "col-2";
                    var ref_2 = document.createElement('a');
                    ref_2.href = "/books/"+data.books[i].pk;
                    ref_2.textContent = data.books[i].name;
                    two_cell_2.appendChild(ref_2);
                    row.appendChild(two_cell_2);

                    var two_cell_3 = document.createElement('div');
                    two_cell_3.className = "col-2";
                    var ref_3 = document.createElement('a');
                    ref_3.href = "/books/"+data.books[i].pk;
                    ref_3.textContent = data.books[i].original_name;
                    two_cell_3.appendChild(ref_3);
                    row.appendChild(two_cell_3);

                    var two_cell_4 = document.createElement('div');
                    two_cell_4.className = "col-4";
                    two_cell_4.textContent = data.books[i].author;
                    row.appendChild(two_cell_4);

                    var two_cell_5 = document.createElement('div');
                    two_cell_5.className = "col-1";
                    two_cell_5.textContent = data.books[i].publish_year;
                    row.appendChild(two_cell_5);

                    var two_cell_6 = document.createElement('div');
                    two_cell_6.className = "col-1";
                    two_cell_6.textContent = data.books[i].rating;
                    row.appendChild(two_cell_6);

                    var two_cell_7 = document.createElement('div');
                    two_cell_7.className = "col-1";
                    row.appendChild(two_cell_7);
                }
            }

        }
        else {
        }
    });
}