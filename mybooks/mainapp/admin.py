from django.contrib import admin
from mainapp.models import Author, Book, Profile, MessageReading, Subscribe, UserActions, CommentForBook
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
# Register your models here.
class BookAdmin(admin.ModelAdmin):
    list_filter = ['name']
    filter_horizontal = ('author',)

class UserProfile(admin.StackedInline):
    model = Profile
    list_filter = ['age']
    can_delete = False
    verbose_name = 'Profile'

class UserAdmin(UserAdmin):
    inlines = (UserProfile, )

class AuthorAdmin(admin.ModelAdmin):
    list_filter = ['name']

admin.site.register(Book, BookAdmin)
admin.site.register(Author, AuthorAdmin)
admin.site.register(MessageReading)
admin.site.register(Subscribe)
admin.site.register(UserActions)
admin.site.register(CommentForBook)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)