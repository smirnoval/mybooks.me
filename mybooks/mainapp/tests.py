from django.test import TestCase
from mainapp.models import Author, Book

class AuthorTestCase(TestCase):
    def setUp(self):
        Author.objects.create(name="Robinson", original_name="Bin", country="USA", birth_date="28.09.90", bigraphy="Shortly", photo_url="http://locust.io")

    def test_author_create(self):
        test_author = Author.objects.get(name="Robinson")
        self.assertEqual(test_author.name, "Robinson")
        self.assertNotEqual(test_author.name, "Binx")

class BookTestCase(TestCase):
    def setUp(self):
        Book.objects.create(
            name="TestBook", original_name="OriginalTestName", book_type="TestType", publish_year="2016", genre="TestGenre",
            annotation="TestAnnotation")

    def test_book_create(self):
        test_book = Book.objects.get(name="TestBook")
        self.assertEqual(test_book.name, "TestBook")
        self.assertNotEqual(test_book.name, "NotTestBook")